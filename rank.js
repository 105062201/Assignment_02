
var rankState = {
    preload: function(){

    },

    create: function(){
        var datacnt=0;
        var rank_post =[];
        var score1=0,score2=0,score3=0,score4=0,score5=0;
        var name1,name2,name3,name4,name5;
        var score1Lable,score2Lable,score3Lable,score4Lable,score5Lable;
        
        game.add.image(0, 0, 'background2'); 

        var titleLabel = game.add.text(game.width/2, 50, 'RANKING', 
        { font: '50px Arial', fill: '#ffffff' }); 
        titleLabel.anchor.setTo(0.5, 0.5);

        var score1Label = game.add.text(game.width/2, 100, 
            '1st: ' + score1 + 'pt  Name: '+ name1, { font: '25px Arial', fill: '#ffffff' }); 
            score1Label.anchor.setTo(0.5, 0.5);
        var score2Label = game.add.text(game.width/2, 150, 
            '2nd: ' + score2 + 'pt  Name: '+ name2, { font: '25px Arial', fill: '#ffffff' }); 
            score2Label.anchor.setTo(0.5, 0.5);
        var score3Label = game.add.text(game.width/2, 200, 
            '3rd: ' + score3 + 'pt  Name: '+ name3, { font: '25px Arial', fill: '#ffffff' }); 
            score3Label.anchor.setTo(0.5, 0.5);
        var score4Label = game.add.text(game.width/2, 250, 
            '4th: ' + score4 + 'pt  Name: '+ name4, { font: '25px Arial', fill: '#ffffff' }); 
            score4Label.anchor.setTo(0.5, 0.5);
        var score5Label = game.add.text(game.width/2, 300, 
            '5th: ' + score5 + 'pt  Name: '+ name5, { font: '25px Arial', fill: '#ffffff' }); 
            score5Label.anchor.setTo(0.5, 0.5);
        
        var scoreRef = firebase.database().ref('score');
        scoreRef.once('value').then(function(snapshot){
            snapshot.forEach(function(childSnapshot){
                var childData = childSnapshot.val();
                if(childData.point>score1){
                    name5=name4; name4=name3; name3=name2; name2=name1;
                    score5=score4; score4=score3; score3=score2; score2=score1;
                    score1=childData.point;
                    name1=childData.name;
                }
                else if(childData.point>score2){
                    name5=name4; name4=name3; name3=name2;
                    score5=score4; score4=score3; score3=score2;
                    score2=childData.point;
                    name2=childData.name;
                }
                else if(childData.point>score3){
                    name5=name4; name4=name3;
                    score5=score4; score4=score3;
                    score3=childData.point;
                    name3=childData.name;
                }
                else if(childData.point>score4){
                    name5=name4;
                    score5=score4;
                    score4=childData.point;
                    name4=childData.name;
                }
                else if(childData.point>score5){
                    score5=childData.point;
                    name5=childData.name;
                }
                else{
                }
                score1Label.setText('1st: ' + score1 + 'pt Name: '+ name1);
                score2Label.setText('2nd: ' + score2 + 'pt Name: '+ name2);
                score3Label.setText('3rd: ' + score3 + 'pt Name: '+ name3);
                score4Label.setText('4th: ' + score4 + 'pt Name: '+ name4);
                score5Label.setText('5th: ' + score5 + 'pt Name: '+ name5);
            })
        }).catch(e => console.log(e.message));
        var backbutton = game.add.button(-50, 260, 'backbtn', actionOnClick4,this);
    },
    
    update:function(){

    }
};

function actionOnClick4 () {
    game.state.start('menu'); 
}
