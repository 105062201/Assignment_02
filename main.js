var game = new Phaser.Game(400,400,Phaser.AUTO,'canvas')
// Define our global variable
game.global = { score: 0 }; 
// Add all the states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('playsingle', playStateSingle);
game.state.add('playdual', playStateDual);
game.state.add('scoreup', scoreupState);
game.state.add('rank', rankState);
// Start the 'boot' state
game.state.start('boot');