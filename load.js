var loadState = { 
    preload: function () {
    // Add a 'loading...' label on the screen

    /*var loadingLabel = game.add.text(game.width/2, 150, 
    'loading...', { font: '30px Arial', fill: '#ffffff' });
    loadingLabel.anchor.setTo(0.5, 0.5);
    // Display the progress bar
    var progressBar = game.add.sprite(game.width/2, 200, 'progressBar'); 
    progressBar.anchor.setTo(0.5, 0.5);
    game.load.setPreloadSprite(progressBar,0);
    */
   
    // Load all game assets
    game.load.audio('jump', 'assets/jump.wav');
    game.load.audio('land', 'assets/land2new.wav');
    game.load.audio('hurt', 'assets/hurt.wav');
    game.load.audio('gamestart', 'assets/gamestart.ogg');
    game.load.audio('death', 'assets/death.wav');

    game.load.image('wall', 'assets/wall.png');
    game.load.image('ceiling', 'assets/ceiling.png');
    game.load.image('normalbar', 'assets/normal.png');
    game.load.image('nails', 'assets/nails.png');
    game.load.image('backbtn', 'assets/backbtnnew.png');
    game.load.image('background', 'assets/backgroundnew.png');
    game.load.image('background2', 'assets/starbackground.gif');
    game.load.spritesheet('player','assets/player.png',32,32);
    game.load.spritesheet('conveyor_l','assets/conveyor_left.png',96,16);
    game.load.spritesheet('conveyor_r','assets/conveyor_right.png',96,16);
    game.load.spritesheet('trampoline','assets/trampoline.png',96,22);
    game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
    game.load.spritesheet('startbtn', 'assets/start.png', 201, 71);
    game.load.spritesheet('rankbtn', 'assets/rankbtn.png', 80, 20);
    // Load a new asset that we will use in the menu state
    //game.load.image('background', 'assets/background.png');
    },
    create: function() {
    // Go to the menu state 
    game.state.start('menu'); 
    } 
};