var player2;
var winner=0,tmp=1;
var text4,text5,text6;
var playStateDual = {
    preload: function(){
        
    },

    create: function(){
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'background'); 
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D
        });
        //this.player = game.add.sprite(game.width/2, game.height/2, 'player');
        createPlayer();
        createPlayer2();
        createBounders();
        createTextsBoard2();
        createSound();
        tmp=1;
    },
    
    update:function(){

        this.physics.arcade.collide(player, platforms, effect);
        this.physics.arcade.collide(player, [leftWall, rightWall]);
        this.physics.arcade.collide(player2, platforms, effect2);
        this.physics.arcade.collide(player2, [leftWall, rightWall]);
        this.physics.arcade.collide(player, player2);
        checkGameOver2();
        checkTouchCeiling(player);
        checkTouchCeiling2(player2);
        updatePlayer();
        updatePlayer2();
        updatePlateforms();
        updateTextsBoard2();
        
        createPlateform();
    }
}


function createPlayer2() {
    player2 = game.add.sprite(150, 50, 'player');
    player2.direction = 10;
    game.physics.arcade.enable(player2);
    player2.body.gravity.y = 500;
    player2.animations.add('left', [0, 1, 2, 3], 8);
    player2.animations.add('right', [9, 10, 11, 12], 8);
    player2.animations.add('jumpleft', [18, 19, 20, 21], 12);
    player2.animations.add('jumpright', [27, 28, 29, 30], 12);
    player2.animations.add('jump', [36, 37, 38, 39], 12);
    player2.life = 10;
    player2.unbeatableTime = 0;
    player2.touchOn = undefined;
}



function createTextsBoard2 () {
    var style = {fill: '#ff0000', fontSize: '20px'}
    text1 = game.add.text(10, 10, '', style);
    text2 = game.add.text(200, 10, '', style);
    text4 = game.add.text(280, 10, '', style);
    text3 = game.add.text(140, 200, 'Enter 重新開始', style);
    text5 = game.add.text(player.body.x , player.body.y ,'1P',{ font: '20px Arial', fill: '#FF0000' });
    text6 = game.add.text(player2.body.x , player2.body.y ,'2P',{ font: '20px Arial', fill: '#FF0000' });
    text3.visible = false;
}


function updatePlayer2(){
    var x = player2.body.velocity.x;
    var y = player2.body.velocity.y;
    if(keyboard.a.isDown){
        player2.body.velocity.x = -200;
    }else if(keyboard.d.isDown){
        player2.body.velocity.x = 200;
    }else{
        player2.body.velocity.x = 0;
    }
    if(x>0 && y>0) player2.animations.play('jumpright');
    else if(x<0 && y>0) player2.animations.play('jumpleft');
    else if(x>0 && y==0) player2.animations.play('right');
    else if(x<0 && y==0) player2.animations.play('left');
    else if(x==0 && y!=0) player2.animations.play('jump');
    else player2.frame = 8;
}


function updateTextsBoard2 () {
    if(winner==2) text1.setText('p1 life: DIED');
    else text1.setText('p1 life:' + player.life);
    
    text2.setText('B' + distance);
    
    if(winner==1)text4.setText('p2 life: DIED');
    else text4.setText('p2 life:' + player2.life);
    
    text5.position.x=player.body.x+8;
    text5.position.y=player.body.y-23;
    text6.position.x=player2.body.x+8;
    text6.position.y=player2.body.y-23;
}


function effect2(player2,platform){
    if(platform.key == 'conveyor_r'){
        player2.body.x +=2;
    }
    else if(platform.key == 'conveyor_l'){
        player2.body.x -=2;
    }
    else if(platform.key == 'trampoline'){
        platform.animations.play('jump');
        player2.body.velocity.y = -350;
        jumpSound.play();
    }
    else if(platform.key == 'nails'){
        if (player2.touchOn !== platform) {
            player2.life -= 3;
            player2.touchOn = platform;
            game.camera.flash(0xff0000, 100);
            hurtSound.play();
        }
    }
    else if(platform.key == 'normalbar'){
        if (player2.touchOn !== platform) {
            if(player2.life < 10) {
                player2.life += 1;
            }
            player2.touchOn = platform;
            landSound.play();
        }
    }
    else if(platform.key == 'fake'){
        if(player2.touchOn !== platform) {
            platform.animations.play('turn');
            setTimeout(function() {
                platform.body.checkCollision.up = false;
            }, 100);
            player2.touchOn = platform;
            landSound.play();
        }
    }
}


function checkTouchCeiling2(player2) {
    if(player2.body.y < 5) {
        if(player2.body.velocity.y <= 0) {
            player2.body.velocity.y = 0;
            player2.body.y=5;
        }
        if(game.time.now > player2.unbeatableTime) {
            player2.life -= 3;
            game.camera.flash(0xff0000, 100);
            player2.unbeatableTime = game.time.now + 2000;
            hurtSound.play();
        }
    }
}

function checkGameOver2 () {
    if(tmp==1){
        if(player.life <= 0 || player.body.y > 500) {winner = 2; tmp=0; player.kill();}
        else if(player2.life <=0 || player2.body.y > 500) {winner = 1; tmp=0; player2.kill();}
        else winner=0;
    }
    
    if((player.life <= 0 && player2.life <=0) ||  (player.body.y > 500 && player2.body.y > 500) || 
    (player.life <= 0 && player2.body.y > 500) || (player2.life <=0 && player.body.y > 500)) {
        gameOver();
    }
}



