var menuState = { 
    create: function() {

    game.add.image(0, 0, 'background2'); 

    var nameLabel = game.add.text(game.width/2, 50, 'DownStairs', 
    { font: '50px Arial', fill: '#ffffff' }); 
    nameLabel.anchor.setTo(0.5, 0.5);

    // Show the score at the center of the screen
    var scoreLabel = game.add.text(game.width/2, 110, 
    'Your score: ' + distance, { font: '25px Arial', fill: '#ffffff' }); 
    scoreLabel.anchor.setTo(0.5, 0.5);

    // Explain how to start the game
    var startLabelSingle = game.add.text(game.width/2, 215, 
        'Single', { font: '25px Arial', fill: '#ffffff' }); 
        startLabelSingle.anchor.setTo(0.5, 0.5);

    var startLabelDual = game.add.text(game.width/2, 315, 
    'Dual', { font: '25px Arial', fill: '#ffffff' }); 
    startLabelDual.anchor.setTo(0.5, 0.5);
    
    // When pressed, call the 'start'
    var button1 = game.add.button(100, 130, 'startbtn', actionOnClick1, this, 2, 0, 1);
    var button2 = game.add.button(100, 230, 'startbtn', actionOnClick2, this, 2, 0, 1); 
    var button3 = game.add.button(310, 350, 'rankbtn', actionOnClick3, this, 2, 0, 1); 

    var rankLabel = game.add.text(325, 351, 
        'Ranking', { font: '15px Arial', fill: '#000000' }); 
        startLabelDual.anchor.setTo(0.5, 0.5);
    },
};    

function actionOnClick1 () {
        game.state.start('playsingle'); 
    }

function actionOnClick2 () {
        game.state.start('playdual'); 
    }

function actionOnClick3 () {
        game.state.start('rank'); 
    }