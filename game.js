var lasttime = 0;
var player,keyboard;
var distance = 0;
var platforms = [];
var text1;
var text2;
var text3;
var jumpSound;
var hurtSound;
var landSound;
var startSound;
var deathSound;
var playStateSingle = {
    preload: function(){
        
    },

    create: function(){
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'background'); 
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D
        });
        //this.player = game.add.sprite(game.width/2, game.height/2, 'player');
        createPlayer();
        createBounders();
        createTextsBoard();
        createSound();
        distance=0;
        winner=0;
    },
    
    update:function(){

        this.physics.arcade.collide(player, platforms, effect);
        this.physics.arcade.collide(player, [leftWall, rightWall]);
        checkGameOver();
        checkTouchCeiling(player);

        updatePlayer();
        updatePlateforms();
        updateTextsBoard();
        
        createPlateform();
    }
}

function createPlayer() {
    player = game.add.sprite(game.width/2, 50, 'player');
    player.direction = 10;
    game.physics.arcade.enable(player);
    player.body.gravity.y = 500;
    player.animations.add('left', [0, 1, 2, 3], 8);
    player.animations.add('right', [9, 10, 11, 12], 8);
    player.animations.add('jumpleft', [18, 19, 20, 21], 12);
    player.animations.add('jumpright', [27, 28, 29, 30], 12);
    player.animations.add('jump', [36, 37, 38, 39], 12);
    player.life = 10;
    player.unbeatableTime = 0;
    player.touchOn = undefined;
}

function createBounders () {
    leftWall = game.add.sprite(0, 0, 'wall');
    game.physics.arcade.enable(leftWall);
    leftWall.body.immovable = true;

    rightWall = game.add.sprite(383, 0, 'wall');
    game.physics.arcade.enable(rightWall);
    rightWall.body.immovable = true;

    ceiling = game.add.image(0, 0, 'ceiling');
}

function createTextsBoard () {
    var style = {fill: '#ff0000', fontSize: '20px'}
    text1 = game.add.text(10, 10, '', style);
    text2 = game.add.text(350, 10, '', style);
    text3 = game.add.text(140, 200, 'Enter 重新開始', style);
    text3.visible = false;
}

function createPlateform(){
    if(game.time.now > lasttime+600){
        lasttime = game.time.now;
        createNewPlateform();
        distance += 1;
    }
}

function createNewPlateform(){
    var x = Math.random()*(400 - 96 - 40) + 20;
    var y = 400;
    var rand = Math.random() *100;

    if(rand < 20) {
        platform = game.add.sprite(x, y, 'normalbar');
    } else if (rand < 40) {
        platform = game.add.sprite(x, y, 'nails');
        game.physics.arcade.enable(platform);
        platform.body.setSize(96, 15, 0, 15);
    } else if (rand < 50) {
        platform = game.add.sprite(x, y, 'conveyor_l');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 60) {
        platform = game.add.sprite(x, y, 'conveyor_r');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 80) {
        platform = game.add.sprite(x, y, 'trampoline');
        platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
        platform.frame = 3;
    } else {
        platform = game.add.sprite(x, y, 'fake');
        platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
    }

    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    platforms.push(platform);

    platform.body.checkCollision.down = false;
    platform.body.checkCollision.left = false;
    platform.body.checkCollision.right = false;
}

function createTextsBoard () {
    var style = {fill: '#ff0000', fontSize: '20px'}
    text1 = game.add.text(10, 10, '', style);
    text2 = game.add.text(350, 10, '', style);
    text3 = game.add.text(140, 200, 'Enter 重新開始', style);
    text3.visible = false;
}

function createSound(){
    jumpSound = game.add.audio('jump');
    landSound = game.add.audio('land');
    landSound.volume += 3;
    hurtSound = game.add.audio('hurt');
    hurtSound.volume += 0.5;
    deathSound = game.add.audio('death');
    startSound = game.add.audio('gamestart');
    startSound.play();
}

function updatePlayer(){
    var x = player.body.velocity.x;
    var y = player.body.velocity.y;
    if(keyboard.left.isDown){
        player.body.velocity.x = -200;
    }else if(keyboard.right.isDown){
        player.body.velocity.x = 200;
    }else{
        player.body.velocity.x = 0;
    }
    if(x>0 && y>0) player.animations.play('jumpright');
    else if(x<0 && y>0) player.animations.play('jumpleft');
    else if(x>0 && y==0) player.animations.play('right');
    else if(x<0 && y==0) player.animations.play('left');
    else if(x==0 && y!=0) player.animations.play('jump');
    else player.frame = 8;
}

function updatePlateforms(){
    for(var i=0; i<platforms.length; i++) {
        var platform = platforms[i];
        platform.body.position.y -= 2;
        if(platform.body.position.y <= -10) {
            platform.destroy();
            platforms.splice(i, 1);
        }
    }
}

function updateTextsBoard () {
    text1.setText('life:' + player.life);
    text2.setText('B' + distance);
}

function effect(player,platform){
    if(platform.key == 'conveyor_r'){
        player.body.x +=2;
        //landSound.play();
    }
    else if(platform.key == 'conveyor_l'){
        player.body.x -=2;
        //landSound.play();
    }
    else if(platform.key == 'trampoline'){
        platform.animations.play('jump');
        player.body.velocity.y = -350;
        jumpSound.play();
    }
    else if(platform.key == 'nails'){
        if (player.touchOn !== platform) {
            player.life -= 3;
            player.touchOn = platform;
            game.camera.flash(0xff0000, 100);
            hurtSound.play();
        }
    }
    else if(platform.key == 'normalbar'){
        if (player.touchOn !== platform) {
            if(player.life < 10) {
                player.life += 1;
            }
            player.touchOn = platform;
            landSound.play();
        }
    }
    else if(platform.key == 'fake'){
        if(player.touchOn !== platform) {
            platform.animations.play('turn');
            landSound.play();
            setTimeout(function() {
                platform.body.checkCollision.up = false;
            }, 100);
            player.touchOn = platform;
        }
    }
}

function checkTouchCeiling(player) {
    if(player.body.y < 5) {
        if(player.body.velocity.y <= 0) {
            player.body.velocity.y = 0;
            player.body.y =5;
        }
        if(game.time.now > player.unbeatableTime) {
            player.life -= 3;
            hurtSound.play();
            game.camera.flash(0xff0000, 100);
            player.unbeatableTime = game.time.now + 2000;
        }
    }
}

function checkGameOver () {
    if(player.life <= 0 || player.body.y > 500) {
        gameOver();
    }
}

function gameOver () {
    platforms.forEach(function(s) {s.destroy()});
    platforms = [];
    deathSound.play();
    game.state.start('scoreup');
}

